export const appConfig =  {
  "DataRoomPrivilege" : [
    {
      "id": 1,
      "cabinetName": "Board Cabinet",
      "cabinetReports": [
        {
          "id": 1,
          "reportName": "Board Papers",
          "path": "/DataRoom/boardCabinet/boardPapers" 
        },
        {
          "id": 2,
          "reportName": "Organizational Reports",
          "path": "/DataRoom/boardCabinet/organisationalReports"  
        },
        {
          "id": 3,
          "reportName": "Special Reports",
          "path": "/DataRoom/boardCabinet/specialReports" 
        }
      ]
    },

    {
      "id": 2,
      "cabinetName": "Senior Management Cabinet",
      "cabinetReports": [
        {
          "id": 1,
          "reportName": "Service Contracts",
          "path": "/DataRoom/seniorManagementCabinet/serviceContracts" 
        },
        {
          "id": 2,
          "reportName": "Licenses and Regulatory",
          "path": "/DataRoom/seniorManagementCabinet/licensesRegulatory"  
        },
        {
          "id": 3,
          "reportName": "Industry Contracts",
          "path": "/DataRoom/seniorManagementCabinet/industryContracts" 
        },
        {
          "id": 4,
          "reportName": "Departmental Reports",
          "path": "/DataRoom/seniorManagementCabinet/departmentalReports" 
        }
      ]
    },

    {
      "id": 3,
      "cabinetName": "Departmental Cabinet",
      "cabinetReports": [
        {
          "id": 1,
          "reportName": "Folder A Name",
          "path": "/DataRoom/departmentalCabinet/folderAName" 
        },
        {
          "id": 2,
          "reportName": "Folder B Name",
          "path": "/DataRoom/departmentalCabinet/folderBName"  
        },
        {
          "id": 3,
          "reportName": "Folder C Name",
          "path": "/DataRoom/departmentalCabinet/folderCName"  
        },
        {
          "id": 4,
          "reportName": "Folder D Name",
          "path": "/DataRoom/departmentalCabinet/folderDName" 
        }
      ]
    }
  ]
}