import logger from './libs/Logger';
import { config } from './config';
import Database from './libs/Database';
import Server from './Server';

// build trigger 15
const server: Server = new Server(config);
const app = server.bootstrap();

const { port, env, mongo } = config;
// build reschedule 1
app.listen(port, () => {
  // DB Connection
  Database.open({ mongoUri: mongo, testEnv: false })
    .then(() => {
      const ann = `|| App is running at port '${port}' in '${env}' mode ||`;

      logger.info(ann.replace(/[^]/g, '-'));
      logger.info(ann);
      logger.info(ann.replace(/[^]/g, '-'));
      logger.info('Press CTRL-C to stop\n');
    });
});
