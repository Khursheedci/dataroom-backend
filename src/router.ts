import logger from './libs/Logger';
import { Router } from 'express';

import {
  userRouter,staffRouter,reportRouter,
} from './controllers';
import { teamRouter } from './controllers/team';

import {kpiRouter} from './controllers/kpi'
import { fromPairs } from 'lodash';

import { activityRegRouter } from './controllers/activityRegistration';
import { dataRoomRouter } from './controllers/dataRoom'
import{submitRouter} from './controllers/submit'
import { agreementRouter } from './controllers/agreement';
import { frameworkRouter } from './controllers/frameWork';
import { kpiTargetRouter } from './controllers/kpiTarget';
import { performanceRouter } from './controllers/performance';

/* tslint:disable: no-var-requires */
const appInfo = require('../package.json');

const router = Router();

router.get('/version', (req, res) => {
  const { version, name, description } = appInfo;
  logger.info(`version = ${version}, name = ${name}, description = ${description}`);

  if (!(typeof version && version)) {
    logger.error('An error occurred while trying to get version: Version not defined');
    res.status(400).send(new Error('Version not defined'));
  }

  res.json({
    description,
    name,
    version,
  });
});

router.use('/users', userRouter);
router.use('/staff', staffRouter);

router.use('/team', teamRouter)
router.use('/report', reportRouter)
router.use('/kpi',kpiRouter)

router.use('/team', teamRouter);
router.use('/report',reportRouter);
router.use('/activity', activityRegRouter);
router.use('/dataRoom', dataRoomRouter);
router.use('/submit', submitRouter);
router.use('/agreement', agreementRouter)
router.use('/kpitarget', kpiTargetRouter)
router.use('/performance', performanceRouter)
router.use('/framework',frameworkRouter)

router.get('/Download',function(req,res) {
  try {
    let folderPath = req.query.folderPath;
    folderPath = folderPath.toString();
    res.sendFile(folderPath);
  } catch (e) {
    console.log(e);
  }
});
  





export default router;
