import { API_METHOD, API_PREFIX } from '../libs/constants';
import { IAuthProvider } from './IAuthProvider';

console.log('        Required ENVS are the following      ');
console.log('AUTH_ACTIVE', process.env.AUTH_ACTIVE);
console.log('GET_AUTHORIZATIONS', process.env.GET_AUTHORIZATIONS);
console.log(' -------------------------------------------- ');

const { GET, POST, PUT, DELETE } = API_METHOD;

const userRules = { isAdmin: true }
export const rules = [
  {
    allow: userRules,
    methods: [GET, DELETE],
    route: `${API_PREFIX}/users/:userId`,
  },
  {
    methods: [GET],
    route: `${API_PREFIX}/users/verify`,
  },
  {
    allow: userRules,
    methods: [GET],
    route: `${API_PREFIX}/users/list`,
  },
  {
    allow: userRules,
    methods: [POST],
    route: `${API_PREFIX}/users/register`,
  },
  {
    allow: userRules,
    methods: [PUT],
    route: `${API_PREFIX}/users/:userId`,
  },
  {
    allow: userRules,
    methods: [DELETE],
    route: `${API_PREFIX}/users/:userId`,
  },
  {
    allow: userRules,
    methods: [GET],
    route: `${API_PREFIX}/users/getDataRoomPriviledge`,
  },

  {
    allow: userRules,
    methods: [POST],
    route: `${API_PREFIX}/staff/register`,
  },
];

const authProvider: IAuthProvider = Object.freeze({
  active: process.env.AUTH_ACTIVE === 'true',
  authentication: {
    secret: process.env.JWT_SECRET,
    tokenLiveTime: Number(process.env.TOKEN_LIFETIME_IN_MIN) || 60,
  },
  authorization: {
    allowWhenNoRule: process.env.AUTHORIZATION_ALLOW_WHEN_NO_RULE === 'true',
    getAuthorizations: process.env.GET_AUTHORIZATIONS === 'true',
    rules,
  },
});


export default authProvider;
