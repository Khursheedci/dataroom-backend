import { config } from 'dotenv';
config();

import logger from '../libs/Logger';
import { IConfig } from './IConfig';
import authProvider from './auth';

const envVars: NodeJS.ProcessEnv = process.env;

logger.info('------', {
  CORS_ORIGIN: envVars.CORS_ORIGIN,
  EMAIL_HOST: envVars.EMAIL_HOST,
  EMAIL_PORT: envVars.EMAIL_PORT,
  EMAIL_FROM: envVars.EMAIL_FROM,
});

const configurations = Object.freeze({
  corsOrigin: envVars.CORS_ORIGIN ? envVars.CORS_ORIGIN.split(',') : ["http://localhost"],
  email: {
    from: envVars.EMAIL_FROM,
    password: envVars.EMAIL_PASSWORD,
    host: envVars.EMAIL_HOST,
    port: Number(envVars.EMAIL_PROT)
  },
  env: envVars.NODE_ENV,
  mongo: envVars.MONGO_URL,
  port: envVars.PORT,
  authProvider,
}) as IConfig;

console.log("::", configurations);

export default configurations;
