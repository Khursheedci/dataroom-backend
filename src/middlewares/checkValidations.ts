import { NextFunction, Response } from 'express';
import { validationResult } from 'express-validator';

// import { AuthRequest } from '../types';
import SystemResponse from '../libs/SystemResponse';

  export default (req:any, res: Response, next: NextFunction) : void => {
  const errors = validationResult(req);

  if (errors.isEmpty()) {
    next();
  } else {
    next(SystemResponse.badRequestError('VALIDATION ERROR', errors.array()))
  }
}


