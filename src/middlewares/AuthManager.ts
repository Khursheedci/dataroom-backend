
import * as jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

import { UserService } from '../services';
import { IAuthProvider } from '../config/IAuthProvider';
import logger from '../libs/Logger';

class AuthManager {
  authConfig: IAuthProvider;
  userService;
  constructor() {
    this.userService = new UserService();
  }

  static instance;
  static getInstance(i) {
    if (!AuthManager.instance) {
      AuthManager.instance = new AuthManager();
    }
    return AuthManager.instance;
  }

  init(authConfig: IAuthProvider) {
    this.authConfig = authConfig;
  }

  authenticate = (headers: any): boolean | { id: string } => {
    const { authorization } = headers;
    const { authentication: { secret } } = this.authConfig;
    if (!authorization) {
      return false;
    }
    try {
      return jwt.verify(authorization.split(' ')[1], secret) as { id: string };
    } catch (err) {
      console.log("::", err)
      return false;
    }
  }

  auth = async (req, res: Response, next: NextFunction) => {
    const { active, authorization: { rules, getAuthorizations, allowWhenNoRule } } = this.authConfig;
    if (!active) {
      return next();
    }
    const { headers, baseUrl, route } = req;
    const matchedRoute = `${baseUrl}${route.path}`;
    const matchingRule = rules.find(rule => rule.route === matchedRoute && rule.methods.includes(req.method));
    logger.info('authorization', headers)
    const authenticatedUser: any = this.authenticate(headers);
    if (!authenticatedUser) {
      return next({ error: 'Unauthorized', message: 'authorization not found', status: 401 });
    }
    const userData = await this.userService.getUserById(authenticatedUser.payload.id);

    if (!userData) {
      return next({ error: 'Unauthorized', message: 'Permission Denied', status: 401 });
    }
    console.log("::", matchingRule, matchedRoute)
    if (!matchingRule) {
      if (allowWhenNoRule) {
        req.user = userData;
        return next();
      } else {
        return next({ error: 'Unauthorized', message: 'Permission Denied', status: 401 });
      }
    }
    const isAutohorized = getAuthorizations ? this.authtorize(matchingRule, userData) : true;
    if (!isAutohorized) {
      return next({ error: 'Unauthorized', message: 'Permission Denied', status: 401 });
    }

    req.user = userData;
    return next();
  }

  authtorize = (rule, user) => {
    try {
      const { allow } = rule;
      if (!allow) {
        return true;
      }
      return ['isAdmin', 'KPIPrivlege', 'reportPrivlege', 'roomPrivlege'].every(val => {
        if (!allow[val]) {
          return true;
        }
        switch (val) {
          case 'roomPrivilege': {
            return user.isAdmin || (user[val] && Array.isArray(user[val]) && allow[val].every(v => user[val].includes(v)))
          }
          case 'reportPrivilege': {
            return user.isAdmin || (user[val] && allow[val] === user[val])
          }
          case 'KPIPrivilege': {
            return user.isAdmin || (user[val] && allow[val] === user[val])
          }
          default: {
            return true;
          }
        }
      })
    } catch (err) {
      logger.error('authorization error', err);
    }
  }
}

export default AuthManager;