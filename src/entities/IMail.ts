export interface IMail {
  from?: string;
  html?: string;
  subject: string;
  text: string;
  to: string;
}
