import AuthManager from './middlewares/AuthManager';
import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';
import * as methodOverride from 'method-override';
import * as morgan from 'morgan';

import { EnvVars } from './libs/constants';
import { notFoundRoute, errorHandlerRoute } from './libs/routes';
import router from './router';

export default class Server {
  private app: express.Express;

  constructor(private config: any) {
    this.app = express();
  }

  get application() {
    return this.app;
  }

  /**
   * To enable all the setting on our express app
   * @returns -Instance of Current Object
   */
  public bootstrap() {
    const { authProvider } = this.config;
    this.initHelmet();
    this.initCompress();
    this.initCookieParser();
    this.initCors();
    this.initSecurity();
    this.initAuth(authProvider);

    this.initJsonParser();
    this.initMethodOverride();
    this.initLogger();

    this.setupRoutes();

    return this.app;
  }

  /**
   * This will Setup all the routes in the system
   *
   * @returns -Instance of Current Object
   * @memberof Server
   */
  public setupRoutes() {
    const { env } = this.config;
    const stack = (env === EnvVars.DEV || env === EnvVars.TEST);
    // mount /health-check
    this.app.use('/health-check', (req, res) => {
      res.send('I am OK');
    });
    // mount all routes on /api path
    this.app.use('/api', router);

    // catch 404 and forward to error handler
    this.app.use(notFoundRoute);

    // error handler, send stacktrace only during development
    this.app.use(errorHandlerRoute(stack));
  }

  /**
   * Compression of the output
   */
  private initCompress() {
    this.app.use(compress());
  }

  /**
   * Parse Cookie header and populate req.cookies with an object keyed by the cookie names
   */
  private initCookieParser() {
    this.app.use(cookieParser());
  }

  private initSecurity() {
    this.app.use((req, res, next) => {
      res.setHeader('Pragma', 'no-cache');
      res.setHeader('Cache-Control', 'no-store, no-cache');
      next();
    });
  }

  /**
   *
   * Lets you to enable cors
   */
  private initCors() {
    this.app.use(cors({
      // methods: 'GET, POST, PUT, DELETE',
      // optionsSuccessStatus: 200,
      origin: "*",
    }));
  }

  /**
   *
   * Helmet helps you secure your Express apps by setting various HTTP headers.
   */
  private initHelmet() {
    this.app.use(helmet());
  }

  /**
   *  - Parses urlencoded bodies & JSON
   */
  private initJsonParser() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  /**
   * Enabling Logger for Development Environment
   */
  private initLogger() {
    this.app.use(morgan('combined'))
  }

  /**
   *
   * Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
   */
  private initMethodOverride() {
    this.app.use(methodOverride());
  }

  /**
   * Initialize auth manager
   */
  private initAuth(authProvider) {
    const authManager: AuthManager = AuthManager.getInstance(1);
    authManager.init(authProvider);
  }
}
