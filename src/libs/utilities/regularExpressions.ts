const yesNoEmptyRegex = /^(Yes|No|)$/;

export const alphaNumSpace = /^[A-Za-z\d\-_\s]*$/;

export const alphaNumSpecSpace = /^[A-Za-z\d\-_!@#\$%\^\&*\):\(+=._-\s]*$/;

export const alphaSpace = /^[A-Za-z\-_\s]*$/;

export const alpha = /^[A-Za-z\d]*$/;

export const bpcCode = /^[a-zA-Z]{2}[0-9]{3}$/;

export const deliveryTime = /^\d*$/;

export const timePeriod = deliveryTime;

export const financialTimePeriod = deliveryTime;
export const mediaTimePeriod = deliveryTime;
export const securityTimePeriod = deliveryTime;

export const dentsuEmail = new RegExp('^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+.)?[a-zA-Z]+.)?'
  + '(dentsuaegis.com|360i.com|amnetgroup.com|dentsu-media.com|dentsumedia.com|dxglobal.com|'
  + 'isobar.com|carat.com|globalloc.com|data2decisions.com|cardinalpath.com|wearefetch.com|'
  + 'firstborn.com|mediagravity.com|gyro.com|icuc.social|icucmoderation.com|iprospect.com|'
  + 'mcgarrybowen.com|merkleinc.com|vizeum.com|posterscope.com|mktg.com|360icanada.com|'
  + 'mitchells.com.au|EMDC1PSPM03.media.global.loc|dentsuimpact.com|aegisdentsunetwork.onmicrosoft.com|'
  + 'iprospect.fr|wis.com.tw|5thfinger.com|mitchcommgroup.com|jellyfishcreative.co.uk|'
  + 'posterscope.de|storylab.com)$');
export const email = /^\w+([%&‘\*+–\\\/=?^`.{|}~\$#\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
export const integer = /^[+-]?[\d]*$/;
export const legalPolicies = /^legalPolicies.*$/;
export const financePolicies = /^financePolicies.*$/;
export const type = /^(Global|Local)$/;
export const contactType = /^(legal|procurement|lead)/;
export const contractType = /^(MSA|MSAL|ST)$/;
export const externalIds = /^[A-Za-z\d\-_!@#\$%\^\&*\):\(+=._-]*$/;
export const contractDuration = /^(Rolling|AutoRenew|Fixed|)$/;
export const accessRight = /^(ProvisionOfServiceOnly|ProvisionOfServicePlusInternal|NoRightsSpecified|)$/;
export const aggregateRight = /^(NoAggregation|WithinClient|AcrossClient|)$/;
export const derivedDataAccess = /^(Unrestricted|ClientOnly|AcrossClientsAnon|)$/;
export const derivedDataOwner = /^(Client|AgencyOwned|)$/;
export const permissions = /^(YesExplicit|YesImplicit|NoExplicit|)$/;
export const ownership = /^(ClientOwned|AgencyOwned|)$/;
export const isDataAnonymous = yesNoEmptyRegex;
export const applyMarketLaw = /^(Yes|NotApplicable|No|)$/;
export const physicalLocationRestriction = /^(YesWithServer|YesWithoutServer|No|)$/;
export const specificSecurityMeasures = yesNoEmptyRegex;
export const stringChecker = /\b[A-Za-z]*(es|ies|ves)\b/;
export const grantApproval = yesNoEmptyRegex;
export const hasAdditionalAgency = yesNoEmptyRegex;
export const hasExplicitRestriction = yesNoEmptyRegex;
export const hasSupportingAgencies = yesNoEmptyRegex;
export const approvedOptions = /^(AgenciesWithinDan|Amnet|ThirdPartyMediaVendors|SoftwareCompanies|StaffWorking|)$/;
export const serviceProvisionOptions = /^(AgenciesAllowed|SupportingDanAgencies|)$/;
export const dataArchival = yesNoEmptyRegex;
export const dataDeliveryObligations = /^(YesWithSpecificTimeFrame|NoTimeFrameSpecified|)$/;
export const formatObligations = /^(YesWithStandardFormat|YesWithSpecificFormat|NoExplicitFormat|)$/;
export const dataRetentionOption = /^(DeleteAfterTermination|DeleteWithinSpecificTime|)$/;
export const explicitAuditFrequency = yesNoEmptyRegex;
export const auditCompliance = yesNoEmptyRegex;
export const auditTrial = yesNoEmptyRegex;
export const explicitReferencedRights = yesNoEmptyRegex;
export const thirdPartyAuditors = yesNoEmptyRegex;
export const pleaseSpecify = alpha;
export const approvalStatus = /^(financeApproving|legalApproving|)$/;
export const sortOrder = /^(asc|desc)$/;
export const sortBy = /^(feedName|vendorName)$/;
export const vendorStatus = /^(active|inActive)$/;

export default {
  'filter.vendorStatus': vendorStatus,
  'sort.client': sortOrder,
  'sort.feed': sortOrder,
  'sort.vendor': sortOrder,
  'sort.vendorStatus': sortOrder,
};
