import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';

import { authConfig } from '../../config';

export const hashPassword = async password => await bcrypt.hash(password, 10);

export const comparePassword = async (plainText, saltedPassword) => bcrypt.compare(plainText, saltedPassword);

export const randomStringGenerator = () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

export const encryptedToken = (payload) => jwt.sign(
  {
    exp: Math.floor(Date.now() / 1000) + 60 * authConfig.authentication.tokenLiveTime,
    payload
  },
  authConfig.authentication.secret,
)