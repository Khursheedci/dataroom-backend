import { flatten, isEqual, transform } from 'lodash';

// To transform the model object (Used in model.ts file)
const toConvert = {
  transform: (doc, ret) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
  virtuals: true,
};

const capitalizeArrayItems = (array: string[]): string[] => array.map((market) => market.toUpperCase());

const cleanObj = (obj) => {
  const cleanedObj = { ...obj };
  Object.keys(cleanedObj).forEach((key) => (cleanedObj[key] === undefined || cleanedObj[key] === null
    || (Array.isArray(cleanedObj[key] && cleanedObj[key].length === 0))) && delete cleanedObj[key]);
  return cleanedObj;
};

const getDuplicateIndexes = (records: object[], propertyNames: string[]): number[] => {
  const filteredCriteria = {};

  records.forEach((element, index) => {
    let value = [];
    let criteria = '';
    propertyNames.forEach((propertyName) => criteria += `${element[propertyName]}:`);

    if (filteredCriteria[criteria]) {
      value = filteredCriteria[criteria];
      value.push(index + 1);
    }

    filteredCriteria[criteria] = value;
  });

  return flatten(Object.keys(filteredCriteria).map((key) => filteredCriteria[key]));
};

const validateJoiSchema = (data: any, schema: any): string[] => {
  const errors = [];
  const option = { abortEarly: false, stripUnknown: true };

  const { error } = schema.validate(data, option);

  if (error) {
    error.details.forEach(({ message }) => errors.push(message));
  }

  return errors;
};

const hasChanges = (prevRecord: object, newRecord: object) => {
  const clonedPrevRecord: any = { ...prevRecord };

  delete clonedPrevRecord.originalId;
  delete clonedPrevRecord._id;
  delete clonedPrevRecord.updatedAt;
  delete clonedPrevRecord.createdAt;
  delete clonedPrevRecord.__v;

  if (!isEqual(clonedPrevRecord, newRecord)) {
    return transform(clonedPrevRecord, (result: object, value: any, key: any) => {
      if (!newRecord[key]) {
        result[key] = undefined;
      }
    });
  }

  return false;
};


export {
  toConvert,
  capitalizeArrayItems,
  cleanObj,
  getDuplicateIndexes,
  validateJoiSchema,
  hasChanges,
};
