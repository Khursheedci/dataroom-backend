import { Types } from 'mongoose';

import * as complexTypeChecker from './complexTypeChecker';
import * as general from './general';
import * as regEx from './regularExpressions';

export const isBoolean = (key, required) => {
  const { message } = general;
  return (value) => {
    complexTypeChecker.isNumber(value, message.invalid(key));
    complexTypeChecker.isObject(value, message.invalid(key));
    if (required && !(value)) {
      throw new Error(message.required(key));
    }
    if (value && typeof value !== 'boolean') {
      throw new Error(message.invalid(key));
    }
    return true;
  };
};

export const isArray = (key, required) => {
  const { message } = general;
  return (value) => {
    if (!required && (typeof value !== 'undefined')) {
      complexTypeChecker.isNumber(value, message.invalid(key));
      complexTypeChecker.isNotArray(value, message.invalid(key));
    }

    if (!required && typeof value === 'undefined') {
      return true;
    }

    if (required && (typeof value === 'undefined')) {
      throw new Error(message.required(key));
    }

    const isValidArray = (value && Array.isArray(value));

    if (!isValidArray || (isValidArray && !value.length)) {
      throw new Error(message.invalid(key));
    }

    return true;
  };
};

export const isObject = (key, required) => {
  const { message } = general;
  return (value) => {
    if (required && (typeof value === 'undefined')) {
      throw new Error(message.required(key));
    }
    if ((typeof value !== 'undefined') && (Array.isArray(value) || typeof value !== 'object')) {
      throw new Error(message.invalid(key));
    }
    return true;
  };
};

export const isValidExternalId = (key, required) => {
  const { message } = general;
  return (value) => {
    complexTypeChecker.isNumber(value, message.invalid(key));
    complexTypeChecker.isObject(value, message.invalid(key));
    if (required && ((typeof value === 'undefined') || (value === ''))) {
      throw new Error(message.required(key));
    }
    if (value !== '') {
      if ((typeof value !== 'undefined') && !regEx.externalIds.test(value)) {
        throw new Error(message.invalid(key));
      }
    }
    return true;
  };
};

export const keyMatchesRegex = (key, required) => {
  const { message } = general;
  return (value) => {
    complexTypeChecker.isNumber(value, message.invalid(key));
    complexTypeChecker.isObject(value, message.invalid(key));
    if (required && !(value && value.length)) {
      throw new Error(message.required(key));
    }
    if ((typeof value !== 'undefined') && !(regEx[key] || regEx.default[key]).test(value)) {
      throw new Error(message.invalid(key));
    }
    return true;
  };
};
export const isValidObjectId = (key) => {
  const { message } = general;
  return (value) => {
    complexTypeChecker.isEmpty(value, message.required(key));
    if (Types.ObjectId.isValid(value)) {
      return true;
    }
    throw new Error(message.invalid(key));
  };
};
