import { customValidator, regEx } from './index';

export default {
  checkBoolean: ({ keyName, required = false }: any) => ({
    custom: {
      options: customValidator.isBoolean(keyName, required),
    },
    in: 'body',
  }),
  checkByRegex: ({ keyName, required = false, in: In = 'body' }: any) => ({
    custom: {
      options: customValidator.keyMatchesRegex(keyName, required),
    },
    in: In,
  }),
  checkExternalId: ({ keyName, in: In = 'body', required = false }: any) => ({
    custom: {
      options: customValidator.isValidExternalId(keyName, required),
    },
    in: In,
  }),
  checkForValidArray: ({ keyName, required = false }: any) => ({
    custom: {
      options: customValidator.isArray(keyName, required),
    },
    in: 'body',
  }),
  checkForValidObject: ({ keyName, required = false }: any) => ({
    custom: {
      options: customValidator.isObject(keyName, required),
    },
    in: 'body',
  }),
  checkIdInParams: ({ keyName, In, optional = false  }: any) => ({
    custom: {
      options: customValidator.isValidObjectId(keyName),
    },
    in: In,
    optional,
  }),
  isValidCountryCode: ({ keyName, optional = false }: any) => ({
    custom: {
      errorMessage: `${keyName} is invalid.`,
      options: (value: string[]) => {
        let isError = false;
        const isArray = Array.isArray(value);
        const isNotEmpty = Boolean(value.length);

        if (isArray && isNotEmpty) {
          const isNotValidMarket = value.some((item: string) => (!item || item.length > 3 || item.length < 2));
          const newValue = value.map((x) => x.toUpperCase());
          const isMarketUnique = ((new Set([...newValue])).size === value.length);

          isError = (!isNotValidMarket && isMarketUnique);
        }

        return isError;
      },
    },
    in: 'body',
    optional,
  }),
};
