import * as nodemailer from 'nodemailer';
import * as smtpTransport from 'nodemailer-smtp-transport';
import config from '../config/configuration';
import { IMail } from '../entities/IMail';

class MailTransporter {
  private transporter: nodemailer.Transporter;

  constructor() {
    const { email: { host, port, from, password} } = config;

    this.transporter = nodemailer.createTransport(
      smtpTransport({
        host,
        port,
        auth: {
          pass: password,
          user: from
        }
      }),
    );
  }

  public async sendMail(options: IMail) {
    return this.transporter.sendMail({ ... options, from: config.email.from });
  }
}

export default new MailTransporter();
