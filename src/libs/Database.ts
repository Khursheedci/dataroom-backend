import { connect, disconnect } from 'mongoose';

import { IDatabaseConfig } from './entities';

class Database {
  public static open({ mongoUri }: IDatabaseConfig) {
    return new Promise((resolve, reject) => {
      // Mongoose options
      const options = {
        bufferMaxEntries: 0,
        keepAlive: 1,
        poolSize: 10, // Maintain up to 10 socket connections
        reconnectInterval: 500, // Reconnect every 500ms
        reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
        useNewUrlParser: true,
      };

      // Mock the mongoose for testing purpose using Mongoose
      // connect to mongo db
      connect(mongoUri, options, (err) => {
        if (err) {
          reject(err);
        }

        resolve(true);
      });
    });
  }

  public static close() {
    disconnect();
  }
}

export default Database;
