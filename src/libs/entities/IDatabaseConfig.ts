interface IDatabaseConfig {
  mongoUri: string;
  testEnv: boolean;
}

export default IDatabaseConfig;
