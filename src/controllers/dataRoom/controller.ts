import { NextFunction, Request, Response } from "express";

import { DataRoomService, UserService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";
import { passwordMethods } from "../../libs/utilities";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class DataRoomController {
  private dataRoomService: DataRoomService;
  private userService: UserService

  constructor() {
    this.dataRoomService = new DataRoomService();
    this.userService = new UserService();
  }

  public register = async (req: any, res: Response, next: NextFunction) => {
    try {
      const body: any = { ... req.body }
      const { dataRoomId, dataRoomPriviledgeId, fileName} = body;
      // console.log(">>>>", req.files, req.file);
// ;      console.log("::::----------", body)
      const result: any = await this.userService.uploadDataRoom(dataRoomId, dataRoomPriviledgeId, req.file, req.file.originalname);
      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_ADD_RECORD);
      } else {
        // console.log(">>>>>>>>>>>>>", body)
        const { folderPath, newfileName } = result;
        body.folderPath = folderPath;
        body.fileId = newfileName
        await this.dataRoomService.create(body);
        return res.send(
          SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
        );
      }  
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const dataRoomList = await this.dataRoomService.getDataRoomList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, dataRoomList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.dataRoomService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.dataRoomService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new DataRoomController();
