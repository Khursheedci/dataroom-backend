import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
  teamName: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isString: {
      errorMessage: " Team Name can only contain alphabets with no space"
    }
  },
  description: {
    in: ["body"],
    errorMessage: "Description is not valid or not provided",
    isString: {
      errorMessage: "Description cannot be empty"
    },
  },
    addMember: {
    in: ["body"],
    isArray: true,
    errorMessage: "This field  is required",
  },
      'addMember.*': {
    isMongoId: true,
    errorMessage: ' Member Id needs to be valid id'
  },
  
    
};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6},
    }
  }
}