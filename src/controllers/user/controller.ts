import { NextFunction, Request, Response } from 'express';

import { UserService } from '../../services';
import SystemResponse from '../../libs/SystemResponse';
import { passwordMethods } from '../../libs/utilities'

import ResultMessages from './constant';
import logger from '../../libs/Logger';
import mailTransport from '../../libs/mailTransporter';

class UserController {
  private userService: UserService;

  constructor() {
    this.userService = new UserService();
  }

  private sendLoginCredentials = (user, password) => {
    return mailTransport.sendMail({
      to: user.email,
      subject: 'Welcome To PMS',
      html: `<h4>Hi ${user.firstName},</h4>
        <p> Welcome to PMS. Please find your password attached below: </p>
        <p> Password: ${password}</p>
      `,
      text: `Hi ${user.firstName} Welcome to PMS. Please find your password attached below. Password: ${password}`
    })
  }

  login = async (req, res, next) => {
    try {
      const { email, password: plainPassword } = req.body; 
      const user = await this.userService.getUserByQuery({ email });
      if (!user) {
        throw SystemResponse.notFoundError(ResultMessages.USERS_LOGIN_FAILED)
      }
      const { password, ...rest } = user;
      const passwordMathced = await passwordMethods.comparePassword(plainPassword, password);
      if (!passwordMathced) {
        throw SystemResponse.notFoundError(ResultMessages.USERS_LOGIN_FAILED)
      }

      return res.send(SystemResponse.success(ResultMessages.USER_LOGIN_SUCCESS, { token: passwordMethods.encryptedToken({ id: rest.originalId, email: email }), userInfo: rest }));
    }
    catch (err) {
      console.log(":::", err);
      return next(err);
    }
  }

  public verify = (req, res) => {
    const { user } = req;
    const { password, ...rest } = user;

    res.send(SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, rest))
  }

  public register = async (req, res: Response, next: NextFunction) => {
    try {
      const { body } = req;
      const existingUser = await this.userService.getUserByQuery({ email: body.email });
      if (existingUser) {
        throw (SystemResponse.badRequestError(ResultMessages.USER_ALREADY_EXIST));
      }
      const bodyHasPassword = body.password ? true : false
      const password = bodyHasPassword ? body.password: passwordMethods.randomStringGenerator();
      console.log("::", password);
      const hashedPassword = await passwordMethods.hashPassword(password);
      if (!bodyHasPassword) {
        await this.sendLoginCredentials(body, password);
      }
      await this.userService.create({ ...body, password: hashedPassword });

      return res.send(SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {}));
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  }
  

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params: { skip = 0, limit = 0 } } = req;
      const userList = await this.userService.getUserList({ skip, limit });

      res.send(SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, userList))
    } catch (err) {
      return next(err);
    }
  }

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.userService.delete(userId);

      if (!result) {
        throw (SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE));
      }

      return res.send(SystemResponse.success(ResultMessages.REMOVED_USER, result));
    } catch (err) {
      return next(err);
    }
  }

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params: { userId }, body } = req;
      const result = await this.userService.update({ ...body, originalId: userId });

      if (!result) {
        throw (SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE));
      }

      return res.send(SystemResponse.success(ResultMessages.REMOVED_USER, result));
    } catch (err) {
      return next(err);
    }
  }

  public getDataRoom = (req: Request, res: Response, next: NextFunction) => {
    try {
      const { params: { userId }, body } = req;
      const result = this.userService.fetchRoomPrivilege();

      if (!result) {
        throw (SystemResponse.notFoundError(ResultMessages.UNABLE_FETCH_ROOM_PRIVILEGE));
      }

      return res.send(SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, result));
    } catch (err) {
      return next(err);
    }
  }

  // For testing

  public upload = async (req: Request, res: Response, next: NextFunction) => {
    try {
      // const { params: { userId }, body } = req;
      // const result = await this.userService.update({ ...body, originalId: userId });

      // if (!result) {
      //   throw (SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE));
      // }

      // return res.send(SystemResponse.success(ResultMessages.REMOVED_USER, result));
      // const result = await this.userService.uploadDataRoom();

      // if (!result) {
      //   throw (SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE));
      // }

      return res.send(SystemResponse.success(ResultMessages.REMOVED_USER, {}));
    } catch (err) {
      return next(err);
    }
  }
}

export default new UserController();
