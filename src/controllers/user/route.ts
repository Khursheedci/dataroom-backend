import AuthManager from '../../middlewares/AuthManager';
import { checkSchema } from 'express-validator';
import { Router } from 'express';

import userController from './controller';
import { registerUserValidation, deleteUserValidations, updateUserValidation, loginValidations } from './validation';
import { checkValidation } from '../../middlewares';

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route('/register')
  .post(
    auth,
    checkSchema(registerUserValidation),
    checkValidation,
    userController.register
  )

router.route('/verify')
  .get(
    auth,
    userController.verify,
  )

router.route('/login')
  .post(
    checkSchema(loginValidations),
    checkValidation,
    userController.login
  )

router.route('/list')
  .get(
    auth,
    userController.list
  )

router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    userController.delete,
  )
  .put(
    auth,
    checkSchema(updateUserValidation),
    checkValidation,
    userController.update,
  );

router.route('/getDataRoomPriviledge')
  .get(
    auth,
    userController.getDataRoom,
  )

router.route('/upload')
  .post(
    // auth,
    userController.upload,
  )

export default router;
