import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
  selectKpiId: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isArray: {
      errorMessage: "KPi Id needs to be valid MongoID",
    },
  },

  

  "selectKpiId.*": {
    isMongoId: true,
    errorMessage: " Member Id needs to be valid id",
  },

  performanceData: {
    in: ["body"],
    isNumeric: true,
    errorMessage: "This field is required",
  },

  selectBy: {
    in: ["body"],
    errorMessage: "Type is not valid or not provided",
    isString: {
      errorMessage: "This field cannot be empty",
    },
  },

  selectById: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isArray: {
      errorMessage: "Needs to be valid MongoID"
    }
  },

  'selectById.*': {
    isMongoId: true,
    errorMessage: ' Needs to be valid id'
  },
};

export const deleteUserValidations: Schema = {
  userId: {
    in: ["params"],
    isMongoId: true,
    errorMessage: "Invalid userId Provided",
  },
};

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
};

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: "Password is required",
    isLength: {
      errorMessage: "Password needs to be minimum of 6 characters",
      options: { min: 6 },
    },
  },
};
