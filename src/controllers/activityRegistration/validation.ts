import { Schema } from "express-validator";

export const activityRegisterValidation: Schema = {
  description: {
    in: ["body"],
    isString:true,
    errorMessage: "This field  is required",
  },

  name: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isString: {
      errorMessage: "Name can only contain alphabets with no space"
    }
  },
   activityBy: {
    in: ["body"],
    isNumeric: true,
    errorMessage: "This field is required",
  },
   
  activityById: {
    in: ["body"],
    isArray: true
  },
    'activityById.*': {
    isMongoId: true,
    errorMessage: ' Activity Id needs to be valid id'
  },

  startDate: {
    in: ["body"],
    isDate: true,
    errorMessage: "This field is required",
  },

  endDate: {
    in: ["body"],
    isDate: true,
    errorMessage: "This field is required",
  },
  
};

export const deleteActivityRegistrationValidation: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateActivityRegistrationValidation: Schema = {
  ...activityRegisterValidation,
  ...deleteActivityRegistrationValidation,
}