import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import activityRegController from "./controller";
import {
  activityRegisterValidation,
  deleteActivityRegistrationValidation,
  updateActivityRegistrationValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route("/register")
  .post(
    auth,
    checkSchema(activityRegisterValidation),
    checkValidation,
    activityRegController.register
  );

router.route('/list')
  .get(
    auth,
    activityRegController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteActivityRegistrationValidation),
    checkValidation,
    activityRegController.delete,
  )
  .put(
    auth,
    checkSchema(updateActivityRegistrationValidation),
    checkValidation,
    activityRegController.update,
  );
export default router;
