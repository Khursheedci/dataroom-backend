import { NextFunction, Request, Response } from "express";

import { ActivityRegService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class ActivityRegController {
  private reportService: ActivityRegService;

  constructor() {
    this.reportService = new ActivityRegService();
  }

  public register = async (req, res: Response, next: NextFunction) => {
    try {
      const { body } = req;
     
      await this.reportService.create({ ...body });

      return res.send(
        SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
      );
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const reportList = await this.reportService.getActivityRegList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, reportList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.reportService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.reportService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new ActivityRegController();
