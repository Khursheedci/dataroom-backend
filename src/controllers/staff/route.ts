import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import staffController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

router
  .route("/register")
  .post(
    auth,
    checkSchema(registerUserValidation),
    checkValidation,
    staffController.register
  );

router.route('/list')
  .get(
    auth,
    staffController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    staffController.delete,
  )
  .put(
    auth,
    checkSchema(updateUserValidation),
    checkValidation,
    staffController.update,
  );
export default router;
