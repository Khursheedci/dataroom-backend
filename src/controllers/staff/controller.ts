import { NextFunction, Request, Response } from "express";

import { StaffService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";
import { passwordMethods } from "../../libs/utilities";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class StaffController {
  private staffService: StaffService;

  constructor() {
    this.staffService = new StaffService();
  }

  public register = async (req, res: Response, next: NextFunction) => {
    try {
      const { body } = req;
      const existingStaff = await this.staffService.getStaffByQuery({
        email: body.email,
      });
      if (existingStaff) {
        throw SystemResponse.badRequestError(ResultMessages.USER_ALREADY_EXIST);
      }

      await this.staffService.create({ ...body });

      return res.send(
        SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
      );
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const staffList = await this.staffService.getStaffList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, staffList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.staffService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.staffService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new StaffController();
