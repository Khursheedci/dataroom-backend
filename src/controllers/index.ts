export { userRouter } from './user';
export { staffRouter } from './staff';
export { teamRouter } from './team';
export { reportRouter } from './report';
export { activityRegRouter } from './activityRegistration';
// export { activityRegRouter } from './activityRegistration';


