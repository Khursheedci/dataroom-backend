import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
frameWorkName: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isString: {
      errorMessage: "frameWork Name can only contain alphabets with no space"
    }
  },

  targetType: {
    in: ["body"],
    isString: {
      errorMessage: "Target Type can not be empty"
    }   
  },

 
   targetTypeId: {
    in: ["body"],
     isArray: true,
    errorMessage: "This field is required",
  },
  'targetTypeId.*': {
    isMongoId: true,
    errorMessage: ' Target Type Id needs to be valid id'
  },

  weight: {
    in: ["body"],
    isNumeric: {
      errorMessage: "Weight field can not be empty"
    }   
  }
  
};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6},
    }
  }
}