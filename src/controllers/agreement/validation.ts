import { Schema } from "express-validator";

export const registerUserValidation: Schema = {
  contractTitle: {
    in: ["body"],
    errorMessage: "Name is not provided",
    isString: {
      errorMessage: "Name can only contain alphabets with no space"
    }
  },
  party: {
    in: ["body"],
    errorMessage: "Last Name is not valid or not provided",
    isString: {
      errorMessage: "Name can only contain alphabets with no space"
    },
  },
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  phoneNumber: {
    in: ["body"],
    errorMessage: "Mobile Number is not valid",
    isNumeric: true,
    isLength: {
      errorMessage: 'PhoneNumber should be of 6 digits',
        // Multiple options would be expressed as an array
        options: { min: 6, max: 6 }
    }
  },
  effectiveDate: {
    in: ['body'],
    errorMessage: 'Description is not valid or not provided',
    isDate: {
      errorMessage: 'Date is required'
    }
  },
  expiryDate: {
    in: ['body'],
    errorMessage: 'Description is not valid or not provided',
    isDate: {
      errorMessage: 'Date is required'
    }
  }

};

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}

export const loginValidations: Schema = {
  email: {
    in: ["body"],
    errorMessage: "Email is required",
    isEmail: true,
  },
  password: {
    in: ["body"],
    isString: true,
    errorMessage: 'Password is required',
    isLength: {
      errorMessage: 'Password needs to be minimum of 6 characters',
      options: { min: 6},
    }
  }
}