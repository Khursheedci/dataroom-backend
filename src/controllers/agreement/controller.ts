import { NextFunction, Request, Response } from "express";

import { AgreementService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";
import { passwordMethods } from "../../libs/utilities";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class AgreementController {
  private agreementService: AgreementService;

  constructor() {
    this.agreementService = new AgreementService();
  }

  public register = async (req, res: Response, next: NextFunction) => {
   
    try {
      const result: any = await this.agreementService.uploadAgreementReport(req.file, req.file.originalname);
      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_ADD_RECORD);
      } else {
        var body = req.body;
        body.folderPath = result['folderPath'];
        body.fileId = result['newfileName'];
        await this.agreementService.create(body);
        return res.send(
          SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
        );
      }  
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const agreementList = await this.agreementService.getAgreementList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, agreementList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.agreementService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.agreementService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new AgreementController();
