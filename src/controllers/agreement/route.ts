import AuthManager from "../../middlewares/AuthManager";
import { checkSchema } from "express-validator";
import { Router } from "express";

import agreementController from "./controller";
import {
  registerUserValidation,
  deleteUserValidations,
  updateUserValidation,
} from "./validation";
import { checkValidation } from "../../middlewares";

const authManager: AuthManager = AuthManager.getInstance(2);
const auth: any = authManager.auth;
const router = Router();

import * as multer from "multer";
const upload = multer({
  limits: {
    fileSize: 1024 * 1024 * 5
  },
});

router
  .route("/register")
  .post(
    auth,
    upload.single('AgreementFile'),
    checkSchema(registerUserValidation),
    checkValidation,
    agreementController.register
  );



router.route('/list')
  .get(
    auth,
    agreementController.list
)
router.route('/:userId')
  .delete(
    auth,
    checkSchema(deleteUserValidations),
    checkValidation,
    agreementController.delete,
  )
  .put(
    auth,
     upload.single('AgreementFile'),
    checkSchema(updateUserValidation),
    checkValidation,
    agreementController.update,
  );
export default router;
