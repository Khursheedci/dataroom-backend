import { Schema } from 'express-validator'

export const registerUserValidation: Schema = {
  reportTypeId: {
    in: ['body'],
    isString: true,
    errorMessage: 'This field  is required'
  },
  // 'reportTypeId.*': {
  //   isMongoId: true,
  //   errorMessage: ' Report Id needs to be valid id'
  // },

   month: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'This field  is required'
  },
   
    submitBy: {
    in: ['body'],
    isString: true,
    errorMessage: 'This field  is required'
  },
  
  year: {
    in: ["body"],
    isNumeric: true,
    errorMessage: "This field is required",
  },
  
  frequency: {
    in: ['body'],
    isNumeric: true,
    errorMessage: 'This field is required'
  },
  status: {
    in: ['body'],
    isString: true,
    errorMessage: 'This field is required'
  },
  reportTypeName: {
    in: ['body'],
    isString: true,
    errorMessage: 'This field is required'
  },
  summary: {
    in: ['body'],
    errorMessage: 'Description is not valid or not provided',
    isString: {
      errorMessage: 'Name can only contain alphabets with no space'
    }
  },

  

}

// export const deleteUserValidations: Schema = {
//   userId: {
//     in: ['params'],
//     isMongoId: true,
//     errorMessage: 'Invalid userId Provided'
//   }
// }

// export const updateUserValidation: Schema = {
//   ...registerUserValidation,
//   ...deleteUserValidations
// }

export const deleteUserValidations: Schema = {
  userId: {
    in: ['params'],
    isMongoId: true,
    errorMessage: 'Invalid userId Provided',
  }
}

export const updateUserValidation: Schema = {
  ...registerUserValidation,
  ...deleteUserValidations,
}