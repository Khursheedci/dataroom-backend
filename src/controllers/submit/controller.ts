import { NextFunction, Request, Response } from "express";

import { SubmitService } from "../../services";
import SystemResponse from "../../libs/SystemResponse";
import { passwordMethods } from "../../libs/utilities";

import ResultMessages from "./constant";
import logger from "../../libs/Logger";

class SubmitController {
  private submitService: SubmitService;

  constructor() {
    this.submitService = new SubmitService();
  }

  public register = async (req, res: Response, next: NextFunction) => {
   
    try {
      const result: any = await this.submitService.uploadSubmitReport(req.file, req.file.originalname);
      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_ADD_RECORD);
      } else {
        var body = req.body;
        body.folderPath = result['folderPath'];
        body.fileId = result['newfileName'];
        await this.submitService.create(body);
        return res.send(
          SystemResponse.success(ResultMessages.USER_CREATED_SUCCESS, {})
        );
      }  
    } catch (err) {
      logger.error(":::::", err);
      return next(err);
    }
  };

  list = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { skip = 0, limit = 0 },
      } = req;
      const submitList = await this.submitService.getSubmitList({ skip, limit });

      res.send(
        SystemResponse.success(ResultMessages.USERS_FETCH_SUCCESS, submitList)
      );
    } catch (err) {
      return next(err);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.params;
      const result = await this.submitService.delete(userId);

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.REMOVED_USER, result)
      );
    } catch (err) {
      return next(err);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const {
        params: { userId },
        body,
      } = req;
      const result = await this.submitService.update({
        ...body,
        originalId: userId,
      });

      if (!result) {
        throw SystemResponse.notFoundError(ResultMessages.UNABLE_TO_REMOVE);
      }

      return res.send(
        SystemResponse.success(ResultMessages.USER_FETCH_SUCCESS, result)
      );
    } catch (err) {
      return next(err);
    }
  };
}

export default new SubmitController();
