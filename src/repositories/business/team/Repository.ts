import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import ITeamModel from './IModel';
import { TeamModel } from './model';

class TeamRepository extends VersioningRepository<ITeamModel, Model<ITeamModel>> {
  constructor() {
    super(TeamModel);
  }

  /**
   * Create team
   * @returns {Team}
   */
  public async create(options: IQueryCreate): Promise<ITeamModel> {
    logger.debug('TeamRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<ITeamModel> {
    logger.debug('TeamRepository - delete: ');
    return super.remove(data);
  }

  public async getTeamByQuery(query: any, projection?: any): Promise<ITeamModel> {
    logger.debug('Team - getTeamsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getTeamById(id: any): Promise<ITeamModel> {
    logger.debug('Team - getTeamById params: ', id);
    return await super.getById(id).lean();
  }

  public async getTeamList(query, projections, options): Promise<Array<ITeamModel>> {
    logger.debug('Team - getTeamById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update team
   * @returns {Team}
   */
  public async update(options: any): Promise<ITeamModel> {
    logger.debug('TeamRepository - update: ');
    return super.update(options);
  }
}

export default TeamRepository;
