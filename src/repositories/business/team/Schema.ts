import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class UserSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      teamName: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      addMember: {
        type: [String],
        required: true,
      },
     
     
    };
    super(baseSchema, options);
  }
}
