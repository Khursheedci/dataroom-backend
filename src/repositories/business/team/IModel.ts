import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  teamName: string;
  description: string;
  addMember:[string]
}
