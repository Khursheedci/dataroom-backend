import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import ISubmitModel from './IModel';
import { SubmitModel } from './model';

class SubmitRepository extends VersioningRepository<ISubmitModel, Model<ISubmitModel>> {
  constructor() {
    super(SubmitModel);
  }

  /**
   * Create submit
   * @returns {Submit}
   */
  public async create(options: IQueryCreate): Promise<ISubmitModel> {
    logger.debug('SubmitRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<ISubmitModel> {
    logger.debug('SubmitRepository - delete: ');
    return super.remove(data);
  }

  public async getSubmitByQuery(query: any, projection?: any): Promise<ISubmitModel> {
    logger.debug('Submit - getSubmitsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getSubmitById(id: any): Promise<ISubmitModel> {
    logger.debug('Submit - getSubmitById params: ', id);
    return await super.getById(id).lean();
  }

  public async getSubmitList(query, projections, options): Promise<Array<ISubmitModel>> {
    logger.debug('Submit - getSubmitById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update submit
   * @returns {Submit}
   */
  public async update(options: any): Promise<ISubmitModel> {
    logger.debug('SubmitRepository - update: ');
    return super.update(options);
  }
}

export default SubmitRepository;
