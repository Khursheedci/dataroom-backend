import { DateTimeFormatType } from 'morgan-body';
import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  reportTypeId: string;
  month: string;
  year: string;
  summary:string;
  folderPath:string;
  fileId:string;
  date: string,
  frequency: number;
  status: string;
  reportTypeName: string;
  submitBy: string;
  isOnTime:Boolean
}
