import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class SubmitReportSchema extends VersionableSchema {


  constructor(options: any) {
    const baseSchema = {
      reportTypeId: {
        type: String,
        required: true,
      },
      month: {
        type: Number,
        required: true,
      },
      year: {
        type: String,
        required: true,
      },
     
      summary: {
        type: String,
        required: true,
      },
      folderPath: {
        type: String
      },
      fileId: {
        type: String,
      },
      date: {
        type: Date,
        required: true
      },
      frequency: {
        type: Number,
        required: true
      },
      status: {
        type: String,
        required: true
      },
      reportTypeName: {
        type: String,
        required: true
      },
      isOnTime: {
        type: Boolean,
        default: true
      },
      submitBy: {
        type: String,
        required: true
      }
    };
    super(baseSchema, options);
  }
}
