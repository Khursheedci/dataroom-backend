import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IReportModel from './IModel';
import ReportSchema from './Schema';

export const submitSchema = new ReportSchema({
  collection: 'submitReports',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
submitSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
submitSchema.method({});

/**
 * Statics
 */
submitSchema.statics = {};

/**
 * Indexes
 */
submitSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Report
 */

const SubmitModel: mongoose.Model<
  IReportModel
> = mongoose.model<IReportModel>(
  'SubmitReport',
  submitSchema,
  'submitReports',
  true,
);

SubmitModel.init();

export { SubmitModel };
