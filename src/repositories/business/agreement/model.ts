import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IAgreementModel from './IModel';
import AgreementSchema from './Schema';

export const agreementSchema = new AgreementSchema({
  collection: 'agreements',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
agreementSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
agreementSchema.method({});

/**
 * Statics
 */
agreementSchema.statics = {};

/**
 * Indexes
 */
agreementSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Agreement
 */

const AgreementModel: mongoose.Model<
  IAgreementModel
> = mongoose.model<IAgreementModel>(
  'Agreement',
  agreementSchema,
  'agreements',
  true,
);

AgreementModel.init();

export { AgreementModel };
