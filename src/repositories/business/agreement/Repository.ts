import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IAgreementModel from './IModel';
import { AgreementModel } from './model';

class AgreementRepository extends VersioningRepository<IAgreementModel, Model<IAgreementModel>> {
  constructor() {
    super(AgreementModel);
  }

  /**
   * Create agreement
   * @returns {Agreement}
   */
  public async create(options: IQueryCreate): Promise<IAgreementModel> {
    logger.debug('AgreementRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IAgreementModel> {
    logger.debug('AgreementRepository - delete: ');
    return super.remove(data);
  }

  public async getAgreementByQuery(query: any, projection?: any): Promise<IAgreementModel> {
    logger.debug('Agreement - getAgreementsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getAgreementById(id: any): Promise<IAgreementModel> {
    logger.debug('Agreement - getAgreementById params: ', id);
    return await super.getById(id).lean();
  }

  public async getAgreementList(query, projections, options): Promise<Array<IAgreementModel>> {
    logger.debug('Agreement - getAgreementById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update agreement
   * @returns {Agreement}
   */
  public async update(options: any): Promise<IAgreementModel> {
    logger.debug('AgreementRepository - update: ');
    return super.update(options);
  }
}

export default AgreementRepository;
