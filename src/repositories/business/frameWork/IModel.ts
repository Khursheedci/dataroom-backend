import { DateTimeFormatType } from 'morgan-body';
import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  frameWorkName: string;
  targetType: [string];
  targetTypeId: string;
  weight: number;  
  
}
