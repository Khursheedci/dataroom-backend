import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IFrameWorkModel from './IModel';
import FrameWorkSchema from './Schema';

export const frameWorkSchema = new FrameWorkSchema({
  collection: 'FrameWork',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
frameWorkSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
frameWorkSchema.method({});

/**
 * Statics
 */
frameWorkSchema.statics = {};

/**
 * Indexes
 */
frameWorkSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Kpi
 */

const frameWorkModel: mongoose.Model<
IFrameWorkModel
> = mongoose.model<IFrameWorkModel>(
  'FrameWork',
  frameWorkSchema,
  'FrameWork',
  true,
);

frameWorkModel.init();

export { frameWorkModel };
