import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IReportModel from './IModel';
import ReportSchema from './Schema';

export const reportSchema = new ReportSchema({
  collection: 'reports',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
reportSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
reportSchema.method({});

/**
 * Statics
 */
reportSchema.statics = {};

/**
 * Indexes
 */
reportSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Report
 */

const ReportModel: mongoose.Model<
  IReportModel
> = mongoose.model<IReportModel>(
  'Report',
  reportSchema,
  'reports',
  true,
);

ReportModel.init();

export { ReportModel };
