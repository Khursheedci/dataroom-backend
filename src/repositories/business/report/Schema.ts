import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class ReportSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      reportName: {
        type: String,
        required: true,
      },
     description: {
        type: String,
        required: true,
      },
      reportTypeBy: {
        type: Number,
        required: true,
      },
       reportTypeId: {
        type: [String],
        required: true,
      },

     frequency: {
        type: Number,
        required: true,
      },
      status: {
        type: Number,
        required: true,
      },
      reportStartDate: {
        type: Date,
        default: Date.now,
        
      },

      gracePeriod: {
        type: Number,
        required: true,
      }

    };
    super(baseSchema, options);
  }
}
