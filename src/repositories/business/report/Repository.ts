import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IReportModel from './IModel';
import { ReportModel } from './model';

class ReportRepository extends VersioningRepository<IReportModel, Model<IReportModel>> {
  constructor() {
    super(ReportModel);
  }

  /**
   * Create report
   * @returns {Report}
   */
  public async create(options: IQueryCreate): Promise<IReportModel> {
    logger.debug('ReportRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IReportModel> {
    logger.debug('ReportRepository - delete: ');
    return super.remove(data);
  }

  public async getReportByQuery(query: any, projection?: any): Promise<IReportModel> {
    logger.debug('Report - getReportsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getReportById(id: any): Promise<IReportModel> {
    logger.debug('Report - getReportById params: ', id);
    return await super.getById(id).lean();
  }

  public async getReportList(query, projections, options): Promise<Array<IReportModel>> {
    logger.debug('Report - getReportById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update report
   * @returns {Report}
   */
  public async update(options: any): Promise<IReportModel> {
    logger.debug('ReportRepository - update: ');
    return super.update(options);
  }
}

export default ReportRepository;
