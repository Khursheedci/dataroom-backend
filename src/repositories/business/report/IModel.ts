import { DateTimeFormatType } from 'morgan-body';
import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  reportName: string;
  reportTypeId: [string];
  reportTypeBy: number;
  description: string;
  frequency : number;
  reportStartDate: Date; 
  status: number;
  gracePeriod:number
  
}
