import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  name: string;
  description: string;
  activityBy: number; 
  activityById: [string];
  activityByName: string;
  startDate: Date;
  endDate: Date;
  milestones: [object]; 
  isCompleted: boolean;
}
