import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IStaffModel from './IModel';
import { ActivityRegModel } from './model';

class ActivityRegRepository extends VersioningRepository<IStaffModel, Model<IStaffModel>> {
  constructor() {
    super(ActivityRegModel);
  }

  /**
   * Create staff
   * @returns {Staff}
   */
  public async create(options: IQueryCreate): Promise<IStaffModel> {
    logger.debug('ActivityRegRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IStaffModel> {
    logger.debug('ActivityRegRepository - delete: ');
    return super.remove(data);
  }

  public async getActivityRegsByQuery(query: any, projection?: any): Promise<IStaffModel> {
    logger.debug('ActivityReg - getActivityRegsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getActivityRegById(id: any): Promise<IStaffModel> {
    logger.debug('ActivityReg - getActivityRegById params: ', id);
    return await super.getById(id).lean();
  }

  public async getActivityRegList(query, projections, options): Promise<Array<IStaffModel>> {
    logger.debug('ActivityReg - getActivityRegList params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update staff
   * @returns {Staff}
   */
  public async update(options: any): Promise<IStaffModel> {
    logger.debug('ActivityRegRepository - update: ');
    return super.update(options);
  }
}

export default ActivityRegRepository;
