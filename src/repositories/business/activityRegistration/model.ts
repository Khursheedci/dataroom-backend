import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IActivityRegModel from './IModel';
import ActivityRegSchema from './Schema';

export const activityRegSchema = new ActivityRegSchema({
  collection: 'activityRegistration',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
activityRegSchema.pre('save', function(next: any) {
  // if (!this['milestones'] || this['milestones'].length == 0) {
  //   this['isCompleted'] = true;
  // } else {
  //   let result = this['milestones'].every( m => m.isCompleted === true);
  //   this['isCompleted'] = result;
  // }
  if (!this['isCompleted']) {
    this['isCompleted'] = false;
  }
  if (this['milestones'] && this['milestones'].length) {
    let result = this['milestones'].every( m => m.isCompleted === true);
    this['isCompleted'] = result;
  }
  next();
});

/**
 * Methods
 */
activityRegSchema.method({});

/**
 * Statics
 */
activityRegSchema.statics = {};

/**
 * Indexes
 */
activityRegSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef ActivityReg
 */

const ActivityRegModel: mongoose.Model<
  IActivityRegModel
> = mongoose.model<IActivityRegModel>(
  'ActivityReg',
  activityRegSchema,
  'activityRegistration',
  true,
);

ActivityRegModel.init();

export { ActivityRegModel };
