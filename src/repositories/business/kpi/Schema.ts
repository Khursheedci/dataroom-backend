import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class ReportSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      kpiName: {
        type: String,
        required: true,
      },
      reportPeriod: {
        type:Number,
        required: true,
      },
     
     description: {
        type: String,
        required: true,
      },
     metric: {
        type: Number,
        required: true,
      },
      reportStartDate: {
        type: Date,
        default: Date.now,
        required: true,
        
      },

       reportEndDate: {
        type: Date,
         default: Date.now,
         required: true,
        
        
      }

      

    };
    super(baseSchema, options);
  }
}
