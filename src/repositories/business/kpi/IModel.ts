import { DateTimeFormatType } from 'morgan-body';
import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  kpiName: string;
  reportPeriod: number;
  description: string;
  reportStartDate: DateTimeFormatType; 
  reportEndDate: DateTimeFormatType;
  metric: number
  
}
