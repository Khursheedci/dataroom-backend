import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IKpiModel from './IModel';
import { KpiModel } from './model';

class KpiRepository extends VersioningRepository<IKpiModel, Model<IKpiModel>> {
  constructor() {
    super(KpiModel);
  }

  /**
   * Create kpi
   * @returns {Kpi}
   */
  public async create(options: IQueryCreate): Promise<IKpiModel> {
    logger.debug('KpiRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IKpiModel> {
    logger.debug('KpiRepository - delete: ');
    return super.remove(data);
  }

  public async getKpiByQuery(query: any, projection?: any): Promise<IKpiModel> {
    logger.debug('Kpi - getKpisByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getKpiById(id: any): Promise<IKpiModel> {
    logger.debug('Kpi - getKpiById params: ', id);
    return await super.getById(id).lean();
  }

  public async getKpiList(query, projections, options): Promise<Array<IKpiModel>> {
    logger.debug('Kpi - getKpiById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update kpi
   * @returns {Kpi}
   */
  public async update(options: any): Promise<IKpiModel> {
    logger.debug('KpiRepository - update: ');
    return super.update(options);
  }
}

export default KpiRepository;
