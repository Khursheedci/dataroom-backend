import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IStaffModel from './IModel';
import { DataRoomModel } from './model';

class DataRoomRepository extends VersioningRepository<IStaffModel, Model<IStaffModel>> {
  constructor() {
    super(DataRoomModel);
  }

  /**
   * Create staff
   * @returns {Staff}
   */
  public async create(options: IQueryCreate): Promise<IStaffModel> {
    logger.debug('DataRoomRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IStaffModel> {
    logger.debug('DataRoomRepository - delete: ');
    return super.remove(data);
  }

  public async getDataRoomsByQuery(query: any, projection?: any): Promise<IStaffModel> {
    logger.debug('DataRoom - getDataRoomsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getDataRoomById(id: any): Promise<IStaffModel> {
    logger.debug('DataRoom - getDataRoomById params: ', id);
    return await super.getById(id).lean();
  }

  public async getDataRoomList(query, projections, options): Promise<Array<IStaffModel>> {
    logger.debug('DataRoom - getDataRoomList params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update staff
   * @returns {Staff}
   */
  public async update(options: any): Promise<IStaffModel> {
    logger.debug('DataRoomRepository - update: ');
    return super.update(options);
  }
}

export default DataRoomRepository;
