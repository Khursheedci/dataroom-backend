import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IDataRoomModel from './IModel';
import DataRoomSchema from './Schema';

export const dataRoomSchema = new DataRoomSchema({
  collection: 'dataRoomistration',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});


dataRoomSchema.pre('save', function(next: any) {
  if (!this['milestones'] || this['milestones'].length == 0) {
    this['isCompleted'] = true;
  } else {
    let result = this['milestones'].every( m => m.isCompleted === true);
    this['isCompleted'] = result;
  }
  next();
});

/**
 * Methods
 */
dataRoomSchema.method({});

/**
 * Statics
*/
dataRoomSchema.statics = {};

/**
 * Indexes
*/
dataRoomSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef DataRoom
 */

const DataRoomModel: mongoose.Model<
  IDataRoomModel
> = mongoose.model<IDataRoomModel>(
  'DataRoom',
  dataRoomSchema,
  'dataRoomistration',
  true,
);

DataRoomModel.init();

export { DataRoomModel };
