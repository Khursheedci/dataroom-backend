import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IStaffModel from './IModel';
import StaffSchema from './Schema';

export const staffSchema = new StaffSchema({
  collection: 'staffs',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
staffSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
staffSchema.method({});

/**
 * Statics
 */
staffSchema.statics = {};

/**
 * Indexes
 */
staffSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Staff
 */

const StaffModel: mongoose.Model<
  IStaffModel
> = mongoose.model<IStaffModel>(
  'Staff',
  staffSchema,
  'staffs',
  true,
);

StaffModel.init();

export { StaffModel };
