import { DRAFT_FINAL } from '../../../libs/constants'
import VersionableSchema from '../../versionable/VersionableSchema'

export default class ReportSchema extends VersionableSchema {
  constructor (options: any) {
    const baseSchema = {
      kpiId: {
        type: [String],
        required: true
      },

       selectBy: {
    type: String,
    required: true
  },

      selectById: {
        type: [String], //1 is staff 2 is team
        required: true
      },

      kpiTarget: {
        type: Number,
        required: true
      },
      reportingCycle: {
        type: Date,
        required: true
        
      },
      actualKpiTarget: {
        type: Number,
        required: true,
        default: 0
        
      }
      
    }

    super(baseSchema, options)
  }
}
