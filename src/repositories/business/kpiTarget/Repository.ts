import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IKpiTargetModel from './IModel';
import { KpiTargetModel } from './model';

class KpiTargetRepository extends VersioningRepository<IKpiTargetModel, Model<IKpiTargetModel>> {
  constructor() {
    super(KpiTargetModel);
  }

  /**
   * Create kpiTarget
   * @returns {KpiTarget}
   */
  public async create(options: IQueryCreate): Promise<IKpiTargetModel> {
    logger.debug('KpiTargetRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IKpiTargetModel> {
    logger.debug('KpiTargetRepository - delete: ');
    return super.remove(data);
  }

  public async getKpiTargetByQuery(query: any, projection?: any): Promise<IKpiTargetModel> {
    logger.debug('KpiTarget - getKpiTargetsByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getKpiTargetById(id: any): Promise<IKpiTargetModel> {
    logger.debug('KpiTarget - getKpiTargetById params: ', id);
    return await super.getById(id).lean();
  }

  public async getKpiTargetList(query, projections, options): Promise<Array<IKpiTargetModel>> {
    logger.debug('KpiTarget - getKpiTargetById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update kpiTarget
   * @returns {KpiTarget}
   */
  public async update(options: any): Promise<IKpiTargetModel> {
    logger.debug('KpiTargetRepository - update: ');
    return super.update(options);
  }
}

export default KpiTargetRepository;
