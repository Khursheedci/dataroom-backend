import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IKpiTargetModel from './IModel';
import KpiTargetSchema from './Schema';

export const kpiTargetSchema = new KpiTargetSchema({
  collection: 'kpiTargets',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
kpiTargetSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
kpiTargetSchema.method({});

/**
 * Statics
 */
kpiTargetSchema.statics = {};

/**
 * Indexes
 */
kpiTargetSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef KpiTarget
 */

const KpiTargetModel: mongoose.Model<
  IKpiTargetModel
> = mongoose.model<IKpiTargetModel>(
  'KpiTarget',
  kpiTargetSchema,
  'kpiTargets',
  true,
);

KpiTargetModel.init();

export { KpiTargetModel };
