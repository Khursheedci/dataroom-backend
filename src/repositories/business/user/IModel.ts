  import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  id: string;
  dataRoomId:[string];
  dataRoomPrivilegeId:[string];
  firstName: string;
  lastName: string;
  email: string;
  phoneCode:number,
  phoneNumber: string;
  password: string;
  isAdmin: boolean;
  reportPrivlege: number;
  KPIPrivlege: number;
  supervisor: [string],
    
}
