import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IUserModel from './IModel';
import UserSchema from './Schema';

export const userSchema = new UserSchema({
  collection: 'users',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
userSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
userSchema.method({});

/**
 * Statics
 */
userSchema.statics = {};

/**
 * Indexes
 */
userSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef User
 */

const UserModel: mongoose.Model<
  IUserModel
> = mongoose.model<IUserModel>(
  'User',
  userSchema,
  'users',
  true,
);

UserModel.init();

export { UserModel };
