import {
  DRAFT_FINAL
} from '../../../libs/constants';
import VersionableSchema from '../../versionable/VersionableSchema';

export default class UserSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },

      phoneCode: {
        type: String,
        required:true,
      
      },

      phoneNumber: {
        type: String,
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
      dataRoomId: {
        type: [String],
        required: true,
      },
   dataRoomPriviledgeId: {
     type: [String],
     required: false,
       
      },


      reportPrivlege: {
        enum: DRAFT_FINAL,
        type: Number,
        required: true,
      },
      KPIPrivlege: {
        enum: DRAFT_FINAL,
        type: Number,
        required: true,
      },
      isAdmin: {
        type: Boolean,
        required: false,
      },

      agreementPrivileges: {
        type: Boolean,
        required: true,
      },
      supervisor: {
        type: [String],
        required: true,
      }
    };
    super(baseSchema, options);
  }
}


