import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IUserModel from './IModel';
import { UserModel } from './model';

class UserRepository extends VersioningRepository<IUserModel, Model<IUserModel>> {
  constructor() {
    super(UserModel);
  }

  /**
   * Create user
   * @returns {User}
   */
  public async create(options: IQueryCreate): Promise<IUserModel> {
    logger.debug('UserRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IUserModel> {
    logger.debug('UserRepository - delete: ');
    return super.remove(data);
  }

  public async getUserByQuery(query: any, projection?: any): Promise<IUserModel> {
    logger.debug('User - getUsersByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getUserById(id: any): Promise<IUserModel> {
    logger.debug('User - getUserById params: ', id);
    return await super.getById(id).lean();
  }

  public async getUserList(query, projections, options): Promise<Array<IUserModel>> {
    logger.debug('User - getUserById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update user
   * @returns {User}
   */
  public async update(options: any): Promise<IUserModel> {
    logger.debug('UserRepository - update: ');
    return super.update(options);
  }
}

export default UserRepository;
