import * as mongoose from 'mongoose';

import { toConvert } from '../../../libs/utilities/helper';
import IPerformanceModel from './IModel';
import PerformanceSchema from './Schema';

export const performanceSchema = new PerformanceSchema({
  collection: 'performances',
  read: 'secondaryPreferred',
  toJSON: toConvert,
  toObject: toConvert,
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
performanceSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Methods
 */
performanceSchema.method({});

/**
 * Statics
 */
performanceSchema.statics = {};

/**
 * Indexes
 */
performanceSchema.index({ originalId: 1, deletedAt: 1 });

/**
 * @typedef Performance
 */

const PerformanceModel: mongoose.Model<
  IPerformanceModel
> = mongoose.model<IPerformanceModel>(
  'Performance',
  performanceSchema,
  'performances',
  true,
);

PerformanceModel.init();

export { PerformanceModel };
