import logger from '../../../libs/Logger';
import { Model } from 'mongoose';
import { VersioningRepository } from '../../versionable';
import { IQueryCreate } from './entities';
import IPerformanceModel from './IModel';
import { PerformanceModel } from './model';

class PerformanceRepository extends VersioningRepository<IPerformanceModel, Model<IPerformanceModel>> {
  constructor() {
    super(PerformanceModel);
  }

  /**
   * Create performance
   * @returns {Performance}
   */
  public async create(options: IQueryCreate): Promise<IPerformanceModel> {
    logger.debug('PerformanceRepository - create: ');
    return super.create(options);
  }

  public async delete(data: any): Promise<IPerformanceModel> {
    logger.debug('PerformanceRepository - delete: ');
    return super.remove(data);
  }

  public async getPerformanceByQuery(query: any, projection?: any): Promise<IPerformanceModel> {
    logger.debug('Performance - getPerformancesByQuery params: ', query, projection);
    return await super.getByQuery(query, projection).lean();
  }

  public async getPerformanceById(id: any): Promise<IPerformanceModel> {
    logger.debug('Performance - getPerformanceById params: ', id);
    return await super.getById(id).lean();
  }

  public async getPerformanceList(query, projections, options): Promise<Array<IPerformanceModel>> {
    logger.debug('Performance - getPerformanceById params: ', options);
    return super.getAll(query, projections, options);
  }

  /**
   * Update performance
   * @returns {Performance}
   */
  public async update(options: any): Promise<IPerformanceModel> {
    logger.debug('PerformanceRepository - update: ');
    return super.update(options);
  }
}

export default PerformanceRepository;
