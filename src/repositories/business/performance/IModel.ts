import { DateTimeFormatType } from "morgan-body";
import IVersionableDocument from "../../versionable/IVersionableDocument";

export default interface IUserModel extends IVersionableDocument {
  id: string;
  performanceData: number;
  selectBy: string;
  selectKpiId: [string];
  selectById:[string]
}
