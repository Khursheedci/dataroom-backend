import { DRAFT_FINAL } from "../../../libs/constants";
import VersionableSchema from "../../versionable/VersionableSchema";

export default class ReportSchema extends VersionableSchema {
  constructor(options: any) {
    const baseSchema = {
      selectBy: {
        type: String, //1 is staff 2 is team
        required: true,
      },
        selectById: {
        type: [String], //1 is staff 2 is team
        required: true,
      },

      selectKpiId: {
        type: [String],
        required: true
      },

      performanceData: {
        type: Number,
        required: true,
      },
    };
    super(baseSchema, options);
  }
}
