export { default as IQueryBaseCreate } from './IQueryBaseCreate';
export { default as IQueryBaseCreateAll } from './IQueryBaseCreateAll';
export { default as IQueryBaseDelete } from './IQueryBaseDelete';
export { default as IQueryBaseGet } from './IQueryBaseGet';
export { default as IQueryBaseList } from './IQueryBaseList';
export { default as IQueryBaseUpdate } from './IQueryBaseUpdate';
