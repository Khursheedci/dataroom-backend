import * as mongoose from 'mongoose';

export const updateVersion = {
  updatedAt: Date,
  updatedBy: String,
};

const id = {
  required: false,
  type: String,
};

const deleteVersion = {
  deletedAt: Date,
  deletedBy: String,
};

const createVersion = {
  createdAt: {
    default: Date.now,
    type: Date,
  },
  createdBy: String,
};

const originalId = {
  required: false,
  type: String,
};

class VersionableSchema extends mongoose.Schema {

  constructor(schema: any, options: any) {
    const versionedSchema = {
      _id: id,
      ...createVersion,
      ...deleteVersion,
      originalId,
      ...updateVersion,
      ...schema,
    };
    super(versionedSchema, options);
  }

}

export default VersionableSchema;
