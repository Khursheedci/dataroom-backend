export default class CacheService {
  /**
   * @description method used to get data in memory cache
   * @param cacheKey String used to get stored data
   * @returns Array cached data
   */
  public static get(cacheKey: string) {
    // return NC.FirstNCache.getInstance(100).get(cacheKey);
  }

  /**
   * @description method set data in memory cache
   * @param cacheKey String used to stored data
   * @param data Any data that need to be cached
   * @returns Array cached data
   */
  public static set(cacheKey: string, data: any) {
    // return NC.FirstNCache.getInstance(100).set(cacheKey, data, 300, true);
  }

  public static clear() {
    // return NC.FirstNCache.getInstance(100).clear();
  }
}
