import { ActivityRegRepository } from '../../repositories';

class ActivityRegService {
  private ActivityRegRepository: ActivityRegRepository;

  public constructor() {
    this.ActivityRegRepository = new ActivityRegRepository();
  }

  public async create(data) {
    return this.ActivityRegRepository.create(data);
  }

  public async delete(data) {
    return this.ActivityRegRepository.delete(data);
  }

  public async getActivityRegsByQuery(query: any): Promise<any> {
    return this.ActivityRegRepository.getActivityRegsByQuery(query);
  }
  public async getActivityRegById(id: string): Promise<any> {
    return this.ActivityRegRepository.getActivityRegById(id);
  }

  public async getActivityRegList(options: any): Promise<any> {
    return this.ActivityRegRepository.getActivityRegList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.ActivityRegRepository.update(data);
  }
}

export default ActivityRegService;
