import { StaffRepository } from '../../repositories';

class StaffService {
  private staffRepository: StaffRepository;

  public constructor() {
    this.staffRepository = new StaffRepository();
  }

  public async create(data) {
    return this.staffRepository.create(data);
  }

  public async delete(data) {
    return this.staffRepository.delete(data);
  }

  public async getStaffByQuery(query: any): Promise<any> {
    return this.staffRepository.getStaffByQuery(query);
  }
  public async getStaffById(id: string): Promise<any> {
    return this.staffRepository.getStaffById(id);
  }

  public async getStaffList(options: any): Promise<any> {
    return this.staffRepository.getStaffList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.staffRepository.update(data);
  }
}

export default StaffService;
