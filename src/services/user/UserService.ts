import { UserRepository } from '../../repositories';
const fs =  require("fs");
import upload from '../uploadService';
const path  = require("path");


const AppConfig = require('../../appConfig.js');
class UserService {
  private userRepository: UserRepository;

  public constructor() {
    this.userRepository = new UserRepository();
  }

  public async create(data) {
    return this.userRepository.create(data);
  }

  public async delete(data) {
    return this.userRepository.delete(data);
  }

  public async getUserByQuery(query: any): Promise<any> {
    return this.userRepository.getUserByQuery(query);
  }
  public async getUserById(id: string): Promise<any> {
    return this.userRepository.getUserById(id);
  }

  public async getUserList(options: any): Promise<any> {
    return this.userRepository.getUserList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.userRepository.update(data);
  }

  public fetchRoomPrivilege() {
    return AppConfig['DataRoomPrivilege'];
  }

  public async uploadDataRoom(dataRoomId, roomPrivlegeId, file, fileName) {
    try {
      return await upload(dataRoomId, roomPrivlegeId, file, fileName);
    } catch(e) {
      throw {'error': 'Error in uploading file'};
    }
  }
}

export default UserService;
