import { KpiTargetRepository } from '../../repositories';

class KpiTargetService {
  private kpiTargetTargetTargetRepository: KpiTargetRepository;

  public constructor() {
    this.kpiTargetTargetTargetRepository = new KpiTargetRepository();
  }

  public async create(data) {
    return this.kpiTargetTargetTargetRepository.create(data);
  }

  public async delete(data) {
    return this.kpiTargetTargetTargetRepository.delete(data);
  }

  public async getKpiTargetByQuery(query: any): Promise<any> {
    return this.kpiTargetTargetTargetRepository.getKpiTargetByQuery(query);
  }
  public async getKpiTargetById(id: string): Promise<any> {
    return this.kpiTargetTargetTargetRepository.getKpiTargetById(id);
  }

  public async getKpiTargetList(options: any): Promise<any> {
    return this.kpiTargetTargetTargetRepository.getKpiTargetList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.kpiTargetTargetTargetRepository.update(data);
  }
}

export default KpiTargetService;
