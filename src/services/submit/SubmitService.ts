import { SubmitReportRepository } from '../../repositories';
// const uploadReport = require("../uploadReportService");
import uploadReport from '../uploadReportService';
class SubmitService {
  private submitRepository: SubmitReportRepository;

  public constructor() {
    this.submitRepository = new SubmitReportRepository();
  }

  public async create(data) {
    return this.submitRepository.create(data);
  }

  public async delete(data) {
    return this.submitRepository.delete(data);
  }

  public async getSubmitByQuery(query: any): Promise<any> {
    return this.submitRepository.getSubmitByQuery(query);
  }
  public async getSubmitById(id: string): Promise<any> {
    return this.submitRepository.getSubmitById(id);
  }

  public async getSubmitList(options: any): Promise<any> {
    return this.submitRepository.getSubmitList({ isAdmin: { $ne: true } }, { password: 0 }, options);
  }

  public async update(data) {
    return this.submitRepository.update(data);
  }

  public async uploadSubmitReport(file, filename) {
    try {
      
      return await uploadReport(file, filename);
    } catch(e) {
      throw {'error': 'Error in uploading file'};
    }
  }
}

export default SubmitService;
